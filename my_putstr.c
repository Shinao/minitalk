/*
** my_putstr.c for my_putstr in /home/monner_r//4day
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Thu Oct  4 10:23:04 2012 raphael monnerat
** Last update Mon Nov 12 11:32:59 2012 raphael monnerat
*/

#include "my.h"

void	my_putstr(char *str)
{
  int i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
}

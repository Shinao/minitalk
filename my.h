/*
** my.h for my.h in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Mon Nov 12 11:22:57 2012 raphael monnerat
** Last update Sat Nov 17 19:41:46 2012 raphael monnerat
*/

#ifndef MY_H_
# define MY_H_

# define ABS(x) (((x) < 0) ? -(x) : x)
# define END_OF_STRING (JUMP * 2 - 1)
# define WAIT (500)
# define START_ASCII (63)
# define JUMP (5)
# define STEP (1)

void	my_putchar(char c);
void	my_putstr(char *str);
void	my_put_nbr(int nbr);
int	my_strlen(char *str);
int	my_getnbr(char *str);

#endif /* !MY_H_ */

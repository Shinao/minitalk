/*
** manage_client.h for manage_client.h in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Tue Nov 13 10:33:38 2012 raphael monnerat
** Last update Tue Nov 13 15:54:26 2012 raphael monnerat
*/

#ifndef MANAGE_CLIENT_H_
# define MANAGE_CLIENT_H_

t_client	*search_client(pid_t pid);
t_client	*create_client(pid_t pid);
void		init_client(t_client *client);
void		add_char_to_message(t_client *client, char c);
void		manage_endof_client(t_client *client);

#endif /* !MANAGE_CLIENT_H_ */

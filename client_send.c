/*
** client_send.c for client_send.c in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Tue Nov 13 19:09:37 2012 raphael monnerat
** Last update Sat Nov 17 19:33:52 2012 raphael monnerat
*/

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "error.h"
#include "client_send.h"

void	send_sig(pid_t pid, int SIGUSR)
{
  usleep(WAIT);
  chk_error(kill(pid, SIGUSR) != -1, "Kill failed", 1, 1);
}

void	send_char(pid_t pid, int jump, int step)
{
  int	j;

  j = 0;
  while (j < jump)
    {
      send_sig(pid, SIGUSR2);
      j = j + 1;
    }
  j = 0;
  if (step == 0)
    send_sig(pid, SIGUSR1);
  else
    while (j < step * 2)
      {
	send_sig(pid, SIGUSR1);
	j = j + 1;
      }
  send_sig(pid, SIGUSR2);
}

void	init_sig(int c, int *jump, int *step, pid_t pid)
{
  if (c >= START_ASCII)
    {
      c = c - START_ASCII;
      send_sig(pid, SIGUSR1);
    }
  else
    {
      c = START_ASCII - c;
      send_sig(pid, SIGUSR2);
    }
  *jump = ABS(c / JUMP);
  *step = ABS(c % JUMP);
}

void	wait_server(int pid)
{
  sig_received = 0;
  while (sig_received == 0)
    {
      chk_error(kill(pid, SIGUSR1) != -1, "Kill failed", 1, 1);
      sleep(1);
    }
  sig_received = 0;
}

void	send_end_sig(pid_t pid)
{
  int	i;

  i = 0;
  while (i < 10)
    {
      send_sig(pid, SIGUSR1);
      i = i + 1;
    }
  wait_server(pid);
}

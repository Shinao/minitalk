/*
** my_putchar.c for my_putchar in /home/monner_r//day03
** 
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
** 
** Started on  Wed Oct  3 19:16:46 2012 raphael monnerat
** Last update Fri Oct 26 11:51:44 2012 raphael monnerat
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}

/*
** client_send.h for client_send.h in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Tue Nov 13 19:10:20 2012 raphael monnerat
** Last update Tue Nov 13 23:49:14 2012 raphael monnerat
*/

#ifndef CLIENT_SEND_H_
# define CLIENT_SEND_H_

void	send_sig(pid_t pid, int SIGUSR);
void	send_char(pid_t pid, int jump, int step);
void	init_sig(int c, int *jump, int *step, pid_t pid);
void	send_end_sig(pid_t pid);
void	wait_server(int pid);

extern int sig_received;

#endif /* !CLIENT_SEND_H_ */

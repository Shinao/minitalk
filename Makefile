##
## Makefile for Makefile in /home/monner_r/SVN/minitalk
## 
## Made by raphael monnerat
## Login   <monner_r@epitech.net>
## 
## Started on  Mon Nov 12 10:51:08 2012 raphael monnerat
## Last update Tue Nov 13 19:19:57 2012 raphael monnerat
##

NAME_SERVER	= server

NAME_CLIENT	= client

SRC_S		= server.c \
		manage_sig.c \
		manage_client.c \
		error.c \
		my_putchar.c \
		my_putstr.c \
		my_put_nbr.c

SRC_C		= client.c \
		client_send.c \
		error.c \
		my_putchar.c \
		my_putstr.c \
		my_strlen.c \
		my_put_nbr.c \
		my_getnbr.c

OBJ_S		= $(SRC_S:.c=.o)

OBJ_C		= $(SRC_C:.c=.o)

CC		= gcc

CFLAGS		= -Wall -pedantic -g

RM		= rm -f

all		: $(NAME_SERVER) $(NAME_CLIENT)

$(NAME_SERVER)	: $(OBJ_S)
		$(CC) -o $(NAME_SERVER) $(OBJ_S)

$(NAME_CLIENT)	:  $(OBJ_C)
		$(CC) -o $(NAME_CLIENT) $(OBJ_C)

clean		:
		$(RM) $(OBJ_S)
		$(RM) $(OBJ_C)

fclean		: clean
		$(RM) $(NAME_SERVER)
		$(RM) $(NAME_CLIENT)

re		: fclean all

/*
** my_put_nbr.c for my_put_nbr in /home/monner_r//piscine/day05
** 
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
** 
** Started on  Fri Oct  5 10:26:43 2012 raphael monnerat
** Last update Sat Oct 27 00:21:18 2012 raphael monnerat
*/

#include "my.h"

void	is_neg(int *nb)
{
  if (*nb < 0)
    {
      my_putchar('-');
      *nb = *nb * -1;
    }
}

void	my_put_nbr(int nb)
{
  int	tmp;

  is_neg(&nb);
  tmp = 1;
  while (nb / tmp > 9)
      tmp = tmp * 10;
  while (tmp >= 1)
    {
      my_putchar((nb / tmp) % 10 + 48);
      tmp = tmp / 10;
    }
}

/*
** manage_client.c for manage_client.c in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Tue Nov 13 10:32:20 2012 raphael monnerat
** Last update Sat Nov 17 19:21:57 2012 raphael monnerat
*/

#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include "server.h"
#include "manage_sig.h"
#include "my.h"
#include "error.h"

void	init_client(t_client *client)
{
  client->init = 0;
  client->step = 0;
  client->ascii = START_ASCII;
}

t_client	*search_client(pid_t pid)
{
  t_client	*tmp_cli;

  tmp_cli = g_client;
  while (tmp_cli != 0 && tmp_cli->pid != pid)
    tmp_cli = tmp_cli->next;
  return (tmp_cli);
}

t_client	*create_client(pid_t pid)
{
  t_client	*new;

  chk_error(kill(pid, SIGUSR1) != -1, "Kill failed", 1, 1);
  new = malloc(sizeof(t_client));
  chk_error(new != 0, "Malloc failed", 1, 1);
  new->pid = pid;
  new->msg = malloc(sizeof(char));
  chk_error(new->msg != 0, "Malloc failed", 1, 1);
  new->msg[0] = '\0';
  new->current = 0;
  init_client(new);
  new->next = g_client;
  g_client = new;
  return (new);
}

void		manage_endof_client(t_client *client)
{
  t_client	*prev;

  chk_error(kill(client->pid, SIGUSR1) != -1, "Kill failed", 1, 1);
  my_putstr(client->msg);
  my_putchar('\n');
  prev = g_client;
  if (client == g_client)
    g_client = g_client->next;
  else
    {
      while (prev->next != client)
	prev = prev->next;
      prev->next = client->next;
    }
  free(client->msg);
  free(client);
  check_sig_callback();
}

void	add_char_to_message(t_client *client, char c)
{
  int	i;
  int	j;
  char	*msg;
  char	*new_msg;

  msg = client->msg;
  i = 0;
  while (msg[i] != '\0')
    i = i + 1;
  new_msg = malloc((i + 2) * sizeof(char));
  chk_error(new_msg != 0, "Malloc failed", 1, 1);
  j = 0;
  while (j <= i)
    {
      new_msg[j] = msg[j];
      j = j + 1;
    }
  new_msg[j - 1] = c;
  new_msg[j] = '\0';
  client->msg = new_msg;
  free(msg);
}

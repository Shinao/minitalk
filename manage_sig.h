/*
** manage_sig.h for manage_sig.h in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Mon Nov 12 11:24:36 2012 raphael monnerat
** Last update Tue Nov 13 17:19:33 2012 raphael monnerat
*/

#ifndef MANAGE_SIG_H_
# define MANAGE_SIG_H_

void	check_sig_callback();
void	move_up(int step, t_client *client);
void	move_down(int step, t_client *client);

#endif /* !MANAGE_SIG_H_ */

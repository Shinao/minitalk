/*
** my_getnbr.c for my_getnbr in /home/monner_r//4day
** 
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
** 
** Started on  Thu Oct  4 12:03:29 2012 raphael monnerat
** Last update Fri Oct 26 11:51:07 2012 raphael monnerat
*/

#include "my.h"

int     puissance(int nb1, int nb2)
{
  int   total;
  int   i;

  total = 1;
  i = 0;
  while (i < nb2)
    {
      total = total * nb1;
      i = i + 1;
    }
  return (total);
}

int	my_getnbr_init(int *i_nb_start, int *i_nb_end, int *nb_neg, char *str)
{
  int	nb_char;
  int	i;

  *i_nb_start = 0;
  *i_nb_end = 0;
  *nb_neg = 0;
  i = 0;
  nb_char = my_strlen(str);
  while (!(str[i] >= 48 && str[i] <= 57) && i != nb_char)
    i = i + 1;
  *i_nb_start = i;
  while ((str[i] >= 48 && str[i] <= 57 && i != nb_char))
    i = i + 1;
  *i_nb_end = i - 1;
  if (*i_nb_start > *i_nb_end)
    return (1);
  i = *i_nb_start - 1;
  while ((str[i] == '-' || str[i] == '+') && i != -1)
    {
      if (str[i] == '-')
	*nb_neg = *nb_neg + 1;
      i = i - 1;
    }
  return (0);
}

int	my_getnbr(char *str)
{
  int	c;
  int	nb;
  int	i_nb_start;
  int	i_nb_end;
  int	nb_neg;

  nb = 0;
  if (my_getnbr_init(&i_nb_start, &i_nb_end, &nb_neg, str))
    return (0);
  while (i_nb_end - i_nb_start >= 0)
    {
      c = ((str[i_nb_start] - '0') * puissance(10, i_nb_end - i_nb_start));
      if (c + nb < nb)
	return (0);
      else
	nb = nb + c;
      i_nb_start = i_nb_start + 1;
    }
  if (nb_neg % 2 == 1)
    nb = nb * -1;
  return (nb);
}

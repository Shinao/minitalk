/*
** error.h for error.h in /home/monner_r/SVN/minishell1
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Wed Nov  7 14:10:15 2012 raphael monnerat
** Last update Thu Nov  8 15:29:15 2012 raphael monnerat
*/

#ifndef ERROR_H_
# define ERROR_H_

void	my_putstr_error(char *str);
int	chk_error(int is_not_error, char *output, int display_error, int end);

#endif /* !ERROR_H_ */

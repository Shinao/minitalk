/*
** manage_sig.c for manage_sig.c in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Mon Nov 12 11:23:42 2012 raphael monnerat
** Last update Sat Nov 17 19:42:01 2012 raphael monnerat
*/

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include "server.h"
#include "my.h"

void		check_sig_callback()
{
  int		busy;
  t_client	*tmp_cli;

  busy = 0;
  tmp_cli = g_client;
  while (tmp_cli != 0 && busy == 0)
    {
      if (tmp_cli->init == 0)
	{
	  usleep(WAIT * 3);
	  kill(tmp_cli->pid, SIGUSR1);
	  tmp_cli->current = 1;
	  busy = 1;
	}
      tmp_cli = tmp_cli->next;
    }
}

void	move_up(int step, t_client *client)
{
  client->ascii = client->ascii + step;
}

void	move_down(int step, t_client *client)
{
  client->ascii = client->ascii - step;
}

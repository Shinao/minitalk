/*
** server.h for server.h in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Mon Nov 12 11:06:21 2012 raphael monnerat
** Last update Tue Nov 13 22:47:07 2012 raphael monnerat
*/

#ifndef SERVER_H_
# define SERVER_H_

#include <sys/types.h>

typedef struct		s_client
{
  pid_t			pid;
  char			ascii;
  char			*msg;
  int			current;
  int			init;
  void			(*move)(int, struct s_client *client);
  int			step;
  struct s_client	*next;
}			t_client;

extern t_client *g_client;

#endif /* !SERVER_H_ */

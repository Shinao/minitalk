/*
** my_strlen.c for my_strlen in /home/monner_r//4day
** 
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
** 
** Started on  Thu Oct  4 10:29:04 2012 raphael monnerat
** Last update Thu Oct  4 10:32:02 2012 raphael monnerat
*/

int	my_strlen(char *str)
{
  int i;

  i = 0;
  while (str[i] != '\0')
    i = i + 1;
  return (i);
}

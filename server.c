/*
** server.c for server.c in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Mon Nov 12 10:55:18 2012 raphael monnerat
** Last update Sat Nov 17 19:16:14 2012 raphael monnerat
*/

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "server.h"
#include "my.h"
#include "manage_sig.h"
#include "manage_client.h"

t_client	*g_client;

void	manage_usr2(t_client *client)
{
  if (client->init == 1)
    {
      if (client->step >= 1)
	{
	  client->step = client->step == 1 ? -1 : -(client->step >> 1);
	  client->move(client->step, client);
	  add_char_to_message(client, client->ascii);
	  init_client(client);
	}
      else
	client->move(JUMP, client);
    }
  else
    {
      client->move = &move_down;
      client->init = 1;
    }
}

void	manage_usr1(t_client *client)
{
  if (client->init != 0)
    {
      client->step = client->step + 1;
      client->move(STEP, client);
      if (client->step >= END_OF_STRING)
	manage_endof_client(client);
    }
  else
    {
      client->move = &move_up;
      client->init = 1;
    }
}

void		catch_usr(int sig, siginfo_t *siginfo, void *context)
{
  t_client	*client;

  if ((client = search_client(siginfo->si_pid)) == 0)
    client = create_client(siginfo->si_pid);
  else if (client->current == 1)
    {
      if (sig == SIGUSR1)
	manage_usr1(client);
      else
	manage_usr2(client);
    }
  if (g_client == client && client->next == 0 && client->current != 1)
    check_sig_callback();
}

int			main(int ac, char **av)
{
  struct sigaction	sigc;

  g_client = 0;
  my_putstr("Server: ");
  my_put_nbr(getpid());
  my_putchar('\n');
  sigc.sa_sigaction = &catch_usr;
  sigc.sa_flags = SA_SIGINFO;
  if (sigaction(SIGUSR1, &sigc, 0) == -1 || sigaction(SIGUSR2, &sigc, 0) == -1)
    return (0);
  while (1)
    pause();
  return (0);
}

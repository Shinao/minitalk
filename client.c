/*
** client.c for client.c in /home/monner_r/SVN/minitalk
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Mon Nov 12 10:56:15 2012 raphael monnerat
** Last update Sat Nov 17 19:35:26 2012 raphael monnerat
*/

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "client_send.h"

int	sig_received;

void	get_sig_usr()
{
  sig_received = 1;
}

int	main(int ac, char **av)
{
  int	i;
  pid_t	pid;
  int	jump;
  int	step;

  if (signal(SIGUSR1, &get_sig_usr) == SIG_ERR)
    return (0);
  if (ac > 2)
    {
      pid = my_getnbr(av[1]);
      wait_server(pid);
      pause();
      i = 0;
      while (av[2][i] != '\0')
	{
	  init_sig(av[2][i], &jump, &step, pid);
	  send_char(pid, jump, step);
	  i = i + 1;
	}
      send_end_sig(pid);
    }
  else
    my_putstr("Usage: ProcessID Message\n");
  return (0);
}

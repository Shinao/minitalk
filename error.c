/*
** error.c for my_ls in /home/monner_r//module/unixc/myls
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Sun Oct 21 15:57:16 2012 raphael monnerat
** Last update Tue Nov 13 19:24:36 2012 raphael monnerat
*/

#include <stdlib.h>
#include <unistd.h>

void	my_putstr_error(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      write(2, &(str[i]), 1);
      i = i + 1;
    }
}

int	chk_error(int is_not_error, char *output, int display_error, int end)
{
  if (!is_not_error)
    {
      if (display_error)
	{
	  my_putstr_error("[Minitalk] ");
	  my_putstr_error(output);
	  my_putstr_error("\n");
	}
      if (end == 1)
	exit(0);
      return (1);
    }
  return (0);
}

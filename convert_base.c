/*
** my_putnbr_base_force.c for my_putnbr_base in /home/monner_r//piscine/day06
**
** Made by raphael monnerat
** Login   <monner_r@epitech.net>
**
** Started on  Tue Oct  9 19:51:50 2012 raphael monnerat
** Last update Mon Nov 12 11:54:01 2012 raphael monnerat
*/

#include <stdlib.h>
#include "my.h"

int	manage_negative(char *nbr, int abs)
{
  int	i;

  if (abs)
    {
      if (nbr[0] == '-')
      {
	i = 0;
	while (nbr[i] != '\0')
	  {
	    nbr[i] = nbr[i + 1];
	    i = i + 1;
	  }
	return (1);
      }
      return (0);
    }
  i = my_strlen(nbr) - 1;
  while (i >= 0)
    {
      nbr[i + 1] = nbr[i];
      i = i - 1;
    }
  nbr[0] = '-';
  return (1);
}

int	my_getnbr_frombase(char *nbr, char *base_from)
{
  int	i;
  int	j;
  int	base_size;
  int	nbr_size;
  int	nbr_result;
  int	nbr_base;
  int	power;

  i = 0;
  base_size = my_strlen(base_from);
  nbr_size = my_strlen(nbr) - 1;
  nbr_result = 0;
  while (nbr_size >= 0)
    {
      j = 0;
      while (base_from[j] != nbr[i])
	j = j + 1;
      power = my_power_rec(base_size, nbr_size);
      nbr_result = nbr_result + (j * power);
      nbr_size = nbr_size - 1;
      i = i + 1;
    }
  return (nbr_result);
}

int	get_malloc_size(int nbr, int base_size)
{
  int	i;

  i = 1;
  while (nbr >= base_size)
    {
      nbr = nbr / base_size;
      i = i + 1;
    }
  return (i);
}

char	*my_getbase_to(int nbr, char *base_to, int is_negative)
{
  int	i;
  char	*result_char;
  int	base_size;
  int	malloc_size;

  i = 0;
  base_size = my_strlen(base_to);
  malloc_size = get_malloc_size(nbr, base_size);
  result_char = malloc((malloc_size + is_negative + 1) * sizeof(char));
  while (nbr >= base_size)
    {
      result_char[i] = base_to[nbr % base_size];
      nbr = nbr / base_size;
      i = i + 1;
    }
  result_char[i] = base_to[nbr % base_size];
  result_char[i + 1] = '\0';
  return (my_revstr(result_char));
}

char	*convert_base(char *nbr, char *base_from, char *base_to)
{
  int	nbr_bridge;
  int	is_negative;
  char	*result_base;

  is_negative = manage_negative(nbr, 1);
  nbr_bridge = my_getnbr_frombase(nbr, base_from);
  result_base = my_getbase_to(nbr_bridge, base_to, is_negative);
  if (is_negative)
    manage_negative(result_base, 0);
  return (result_base);
}
